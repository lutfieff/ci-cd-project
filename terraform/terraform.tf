terraform {
  backend "s3" {
    bucket = "lab-project-1"
    region = "ap-south-1"
    key = "labs"
    access_key = $AWS_ACCESS_KEY_ID
    secret_key = $AWS_SECRET_ACCESS_KEY
  }
}
