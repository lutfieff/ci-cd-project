provider "aws" {
  region = "ap-northeast-2"
  access_key = $AWS_ACCESS_KEY_ID
  secret_key = $AWS_SECRET_ACCESS_KEY
}

module "networking" {
  source    = "./modules/networking"
  project = "StudyCase-7"
  owner   = "Kelompok-11"

  cidr_block                 = "10.0.0.0/16"
  private_subnet_cidr_blocks = ["10.0.1.0/24", "10.0.3.0/24"]
  public_subnet_cidr_blocks  = ["10.0.0.0/24", "10.0.2.0/24"]

  environment = "Production"
}

module "instances" {
  source      = "./modules/instances"
  depends_on  = [module.networking]
  vpc         = module.networking.vpc
  sg          = module.networking.sg
  sub_pvt     = module.networking.private_subnet_ids
  sub_pub     = module.networking.public_subnet_ids
  project     = "StudyCase-7"
  owner       = "Kelompok-11"
  environment = "Production"
}
