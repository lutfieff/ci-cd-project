variable "project" {
  type        = string
  description = "Name of project this VPC is meant to house"
}

variable "owner" {
  type        = string
  description = "Owner of project"
}

variable "environment" {
  type        = string
  description = "Name of environment this VPC is targeting"
}

variable "tags" {
  type = map(string)
  default = {
    ManagedBy = "Terraform"
  }
}

variable "ami" {
  type    = string
  default = "ami-0df99b3a8349462c6"
  description = "AMI type of VM"
}

variable "key" {
  type    = string
  default = "key-11"
}

variable "vpc" {
  type = any
}

variable "sg" {
  type = any
}

variable "sub_pvt" {
  type = any
}

variable "sub_pub" {
  type = any
}

variable "private_key_file_path" {
  type        = string
  description = "The location of the key-pair pem file"
  default     = "~/.ssh/key-11.pem"
}

variable "ssh_user" {
  type        = string
  default     = "ubuntu"
}
