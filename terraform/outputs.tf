output "ip_cms" {
  value       = module.instances.ip_cms
  description = "Private IP of CMS Server"
}

output "alb_web_dns" {
  value       = module.instances.alb_web_dns
  description = "DNS name of ALB"
}

output "ip_cache" {
  value       = module.instances.ip_cache
  description = "Private IP of Web Server"
}

output "ip_db" {
  value       = module.instances.ip_db
  description = "Private IP of DB Server"
}

output "ip_lb-db" {
  value       = module.instances.ip_lb-db
  description = "Private IP of LB DB Server"
}

output "ip_thumbor" {
  value       = module.instances.ip_thumbor
  description = "Private IP of Thumbor Server"
}

output "ip_bastion" {
  value       = module.instances.ip_bastion
  description = "Public IP of Bastion Server"
}

output "ip_elk" {
  value       = module.instances.ip_elk
  description = "Private IP of ELK Server"
}

